import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:library_upload_document/src/upload_new_document_provider.dart';

class UploadNewDocumentPackageView extends StatefulWidget {
  const UploadNewDocumentPackageView(
      this.env,this.docId,
      {Key? key}) : super(key: key);
  final String env;
  final String docId;

  @override
  _UploadNewDocumentViewState createState() => _UploadNewDocumentViewState();
}

class _UploadNewDocumentViewState extends State<UploadNewDocumentPackageView> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  UploadNewDocumentProvider _provider= UploadNewDocumentProvider();
  final ImagePicker _picker = ImagePicker();
  String? _directoryPath;
  bool _loadingPath = false;
  bool isCamera=false;
  final itemSize =350.0;

  ScrollController? _scrollController;
  TextEditingController descriptionController= TextEditingController();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  XFile? cameraImage;


  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }
  void dispose(){
    // Don't forget to dispose the ScrollController.
    _scrollController!.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<UploadNewDocumentProvider>(
      create: (context) => _provider,
      child: Scaffold(
        key: _scaffoldKey,
        appBar:AppBar(
          leading :  Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: IconButton(
              icon: Image.asset(ImagePath.leftArrowIcon),
              onPressed: () {
                Navigator.of(context).pop(true);
                // _scaffoldKey.currentState!.openDrawer();// do something
              },
            ),
          ),
          // centerTitle: false,
          title: Text(
            AppConstants.uploadNewDocumentAppBar,
            style: boldTextStyle.copyWith(fontSize: 18),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        // extendBodyBehindAppBar: true,
        body: Consumer<UploadNewDocumentProvider> (
            builder: (context,model,child){
              return Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(ImagePath.commonBackground),
                      fit: BoxFit.cover,
                    )),
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Column(
                    children: [
                      Expanded(
                        child: Form(
                          key: _formKey,
                          child: GestureDetector(
                            onTap: () {
                              FocusScopeNode currentFocus = FocusScope.of(context);
                              if (!currentFocus.hasPrimaryFocus &&
                                  currentFocus.focusedChild != null) {
                                FocusManager.instance.primaryFocus!.unfocus();
                              }
                            },
                            child: ListView(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0,right: 24,top: 24),
                                  // child: Text(AppConstants.enterDocumentId, style: boldTextStyle,),
                                  child: Text('Document Id', style: boldTextStyle,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0,right: 24, top: 8),
                                  child: Text(widget.env),
                                  // child: TextField(),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0,right: 24),
                                  child: Divider(color: Colors.black,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0,right: 24,top: 32),
                                  child: Text(AppConstants.enterDescription, style: boldTextStyle,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0,right: 24),
                                  child: TextFormField(
                                    controller: descriptionController,
                                    textInputAction: TextInputAction.done,
                                    validator: (value) {
                                      if (value!.isEmpty || value == null) {
                                        return AppConstants.pleaseEnterDescription;
                                      }
                                    },
                                    maxLines: 4,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0,right: 24,top: 24),
                                  child: InkWell(
                                    onTap:(){
                                      showMyAlertDialog(context, model);
                                    },
                                    autofocus: true,
                                    child: Container(
                                      height: 58,
                                      width: 390,
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(Radius.circular(4.0)),
                                        color: AppColors.colorBlue,
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(16.0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('${AppConstants.attachDocumentBtnName}',
                                              style: titleHeadline.copyWith(
                                                color: AppColors.colorWhite,),),
                                            ImageIcon(
                                              AssetImage(ImagePath.upwardArrowIcon),
                                              color: AppColors.colorWhite,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                // SizedBox(height: 8,),
                                Stack(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 24.0,right: 24),
                                      child: Container(
                                        child:   model.imageNames.length==0
                                        // _imageFileList == null
                                            ? Container(
                                          height: 163,
                                          //                             child: ListView.builder(
                                          //                                 scrollDirection: Axis.horizontal,
                                          //                                 controller: _scrollController,
                                          //                                 // itemExtent: itemSize,
                                          //                                 // itemCount: galleryPath!.length,
                                          //                                 itemCount: 3,
                                          //                                 itemBuilder: (BuildContext context, int index) {
                                          //                                   return Row(
                                          //                                     children: [
                                          //                                       Padding(
                                          //                                         padding: const EdgeInsets.all(8.0),
                                          //                                         child:Stack(
                                          //                                           children:[
                                          //                                         Container(
                                          //                                           height: 130,
                                          //                                           width: 163,
                                          //                                           // color: Colors.white54,
                                          //                                           decoration: BoxDecoration(
                                          //                                               borderRadius: BorderRadius.circular(8),
                                          //                                               color: Colors.white
                                          //                                           ),
                                          //                                           child: new Align(
                                          //                                             alignment: Alignment.bottomCenter,
                                          //                                             child: Stack(
                                          //                                               children: <Widget>[
                                          //                                                 Container(
                                          //                                                   padding: EdgeInsets.all(8),
                                          //                                                   margin: EdgeInsets.only(top: 0.0),
                                          //                                                   height: MediaQuery.of(context).size.height * 0.3,
                                          //                                                   width: MediaQuery.of(context).size.width * 10,
                                          //                                                   child: Container(
                                          //                                                     decoration: BoxDecoration(
                                          //                                                       borderRadius: BorderRadius.circular(8),
                                          //                                                       color: Colors.grey,
                                          //                                                     ),
                                          //                                                     child: Image.asset(
                                          //                                                       ImagePath.blankImage,
                                          //                                                       fit: BoxFit.fill,),
                                          //                                                   ),
                                          //                                                 ),
                                          //
                                          //                                               ],
                                          //                                             ),
                                          //                                           ),
                                          //                                         ),
                                          //                                         Positioned(
                                          //                                           // left: 0.5,
                                          //                                           right: 0.1,
                                          //                                           top: 0.0,
                                          //
                                          //                                           child: Container(
                                          //                                             decoration: BoxDecoration(
                                          //                                               borderRadius:
                                          //                                               BorderRadius.circular(100),
                                          //                                               // border: Border.all(width: 2, color: Colors.blue)
                                          //                                             ),
                                          //                                             child: CircleAvatar(
                                          //                                               radius: 16,
                                          //                                               backgroundColor: Colors.white,
                                          //                                               child: CircleAvatar(
                                          //                                                 backgroundColor: Colors.white,
                                          //                                                 child: IconButton(
                                          //                                                   icon: Image.asset(
                                          //                                                       ImagePath.deleteIcon),
                                          //                                                   onPressed: () {
                                          //                                                     setState(() {
                                          //                                                       //model.imageNames.removeAt(index);
                                          //                                                     });
                                          //                                                   },
                                          //                                                 ),
                                          //                                               ),
                                          //                                             ),
                                          //                                           ),
                                          //                                         ),
                                          // ]
                                          //                                         ),
                                          //                                       ),
                                          //                                     ],
                                          //                                   );
                                          //                                 }
                                          //                             ),
                                        )
                                            : Container(
                                          height: 163,
                                          child: ListView.builder(
                                              scrollDirection: Axis.horizontal,
                                              controller: _scrollController,
                                              // itemExtent: itemSize,
                                              // itemCount: _imageFileList!.length,
                                              itemCount: model.imageNames.length,
                                              itemBuilder: (BuildContext context, int index) {
                                                return Row(
                                                  children: [
                                                    Padding(
                                                      padding: const EdgeInsets.all(8.0),
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            height: 130,
                                                            width: 163,
                                                            // color: Colors.white54,
                                                            decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.circular(8),
                                                                color: Colors.white
                                                            ),
                                                            child: new Align(
                                                              alignment: Alignment.bottomCenter,
                                                              child: Stack(
                                                                children: <Widget>[
                                                                  Container(
                                                                    padding: EdgeInsets.all(8),
                                                                    margin: EdgeInsets.only(top: 0.0),
                                                                    height: MediaQuery.of(context).size.height * 0.3,
                                                                    width: MediaQuery.of(context).size.width * 10,
                                                                    child: Container(
                                                                      decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(8),
                                                                        color: Colors.grey,
                                                                      ),
                                                                      child:
                                                                      // Image.file(File(_imageFileList![index].path),
                                                                      //   fit: BoxFit.fill,
                                                                      // ),
                                                                      Image.file(
                                                                        File(model.imageNames[index].path),
                                                                        fit: BoxFit.fill,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Positioned(
                                                                    // left: 0.5,
                                                                    right: 0.5,
                                                                    top: 0.0,
                                                                    child: Container(
                                                                      decoration: BoxDecoration(
                                                                        borderRadius:
                                                                        BorderRadius.circular(100),
                                                                        // border: Border.all(width: 2, color: Colors.blue)
                                                                      ),
                                                                      child: CircleAvatar(
                                                                        radius: 16,
                                                                        backgroundColor: Colors.white,
                                                                        child: CircleAvatar(
                                                                          backgroundColor: Colors.white,
                                                                          child: IconButton(
                                                                            icon: Image.asset(
                                                                                ImagePath.deleteIcon),
                                                                            onPressed: () {
                                                                              model.deleteImages(index);
                                                                              // model.imageNames.removeAt(index).path;
                                                                            },
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                );
                                              }
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      left: 8,
                                      // right: 0.5,
                                      top: 60.0,
                                      child: Container(

                                        decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(100),
                                          // border: Border.all(width: 2, color: Colors.blue)
                                        ),
                                        child: CircleAvatar(
                                          radius: 24,
                                          backgroundColor: Colors.white,
                                          child: IconButton(
                                            icon: Image.asset(
                                                ImagePath.leftArrow),
                                            onPressed: () {

                                              if(model.imageNames.length==0){
                                                _scrollController!.animateTo(_scrollController!.offset - 3,
                                                    curve: Curves.linear,
                                                    duration: Duration(milliseconds: 500)
                                                );
                                              }
                                              _scrollController!.animateTo(_scrollController!.offset - itemSize,
                                                  curve: Curves.linear,
                                                  duration: Duration(milliseconds: 500)
                                              );
//                  con.jumpTo(con.offset - itemSize);
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      right: 8,
                                      top: 60.0,
                                      child: Container(

                                        decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.circular(100),
                                          // border: Border.all(width: 2, color: Colors.blue)
                                        ),
                                        child: CircleAvatar(
                                          radius: 24,
                                          backgroundColor: Colors.white,
                                          child: IconButton(
                                            icon: Image.asset(
                                                ImagePath.rightArrow),
                                            onPressed: () {
                                              if(model.imageNames.length==0){
                                                _scrollController!.animateTo(_scrollController!.offset + 3,
                                                    curve: Curves.linear,
                                                    duration: Duration(milliseconds: 500)
                                                );
                                              }
                                              _scrollController!.animateTo(_scrollController!.offset + itemSize,
                                                  curve: Curves.linear,
                                                  duration: Duration(milliseconds: 500)
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                    ),

                                  ],
                                ),

                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0,right: 24),
                                  child: Text(AppConstants.documentList, style: titleHeadline.copyWith(fontSize: 15, color: AppColors.colorBlue),),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 24.0,right: 24),
                                  child: Divider(
                                    color: AppColors.colorOrange,
                                    thickness: 1.5,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0,right: 8),
                                  child: Builder(
                                    builder: (BuildContext context) =>
                                    _loadingPath
                                        ? Padding(
                                      padding: const EdgeInsets.only(bottom: 10.0),
                                      // child: const CircularProgressIndicator(),
                                    )
                                        : _directoryPath != null
                                        ? ListTile(
                                      title: const Text('Directory path'),
                                      subtitle: Text(_directoryPath!),
                                    )
                                        : model.selectedPdfPath != null
                                        ? Container(
                                      padding: const EdgeInsets.only(bottom: 30.0),
                                      height:
                                      MediaQuery.of(context).size.height * 0.30,
                                      child: ListView.builder(
                                        itemCount:
                                        model.selectedPdfPath != null
                                            ? 1
                                            : 0,
                                        itemBuilder: (BuildContext context, index) {
                                          // final bool isMultiPath = model.selectedPdfPath != null;
                                          // final String name = 'Document $index: ' +
                                          //     (isMultiPath ? model.selectedPdfPath!.map((e) => e.name).toList()[index] : model.fileName ?? '...');
                                          // final path = model.selectedPdfPath!.map((e) => e.path).toList()[index].toString();

                                          String name = 'Document ${index+1}:' +model.selectedPdfPath!.name;
                                          return Container(
                                            height: 40,
                                            child: ListTile(
                                              title: Text(
                                                name, style: HeadlineTextStyle,
                                              ),
                                              trailing: Container(
                                                height: 40,
                                                width: 40,
                                                child: IconButton(
                                                  icon: Image.asset(ImagePath.deleteIcon),
                                                  onPressed: () {
                                                    model.deletePdfFile();
                                                    // _selectedPdfPath!.map((e) => e.name).toList().removeAt(index);
                                                  },
                                                ),
                                              ),
                                              // subtitle: Text(path),
                                            ),
                                          );
                                        },
                                      ),
                                    ) : const SizedBox(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 24.0,right: 24,bottom: 24),
                          child: CustomButton(
                            height: 58,
                            minWidth: 390,
                            buttonName: AppConstants.submitBtn,
                            onPressed: () {
                              // model.selectImageFromGallery![0].path, model.selectedPdfPath![0].path
                              FocusScopeNode currentFocus = FocusScope.of(context);
                              if (!currentFocus.hasPrimaryFocus &&
                                  currentFocus.focusedChild != null) {
                                FocusManager.instance.primaryFocus!.unfocus();
                              }
                              if (_formKey.currentState!.validate()) {
                                if ((model.selectImageFromGallery != null && model.selectImageFromGallery!.isNotEmpty) ||
                                    (cameraImage != null && cameraImage!.path.isNotEmpty) ||
                                    (model.selectedPdfPath != null)) {
                                  submitDocument();
                                }else {
                                  CustomToast().customToast(
                                      'Please select document');
                                }
                              }
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
        ),
      ),
    );

  }

  showMyAlertDialog(BuildContext context, UploadNewDocumentProvider model) {
    // var javascript = ProgrammingLanguage("Javascript", 67.7);
    // var htmlCss = ProgrammingLanguage("HTML/CSS", 63.1);
    // var sql = ProgrammingLanguage("SQL", 57.4);
    SimpleDialog dialog = SimpleDialog(
      title: const Text(AppConstants.selectDocument),
      children: <Widget>[
        SimpleDialogOption(
            onPressed: () {
              imagePicker(model);
              // Navigator.pop(context, javascript);
              Navigator.pop(context,imagePicker);
            },
            child: Text(AppConstants.camera)
        ),
        SimpleDialogOption(
          onPressed: () {
            selectImagesFromGallery(model);
            Navigator.pop(context);
          },
          child:  Text(AppConstants.gallery),
        ),
        SimpleDialogOption(
          onPressed: () {
            pdfPicker(model);
            Navigator.pop(context);
          },
          child:  Text(AppConstants.documents),
        )
      ],
    );

    Future futureValue = showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        }
    );
    futureValue.then( (language) => {
      this.setState(() {
        // this.selectedLanguage = language;
      })
    });
  }

  imagePicker(UploadNewDocumentProvider model) async {
    model.deletePdfFile();
    cameraImage = (await _picker.pickImage(source: ImageSource.camera))!;
    print('path for image ${cameraImage!.path}');
    isCamera=true;
    model.addImage(cameraImage!);
    // _image = image;
    // model.imageNames.insert(0, _image!);
    // model.imageNamesCount!.insert(0,0);
    print('Images Frm camera Path==== ${cameraImage!.path}');
  }
  void selectImagesFromGallery(UploadNewDocumentProvider model) async {
    model.deletePdfFile();
    _provider.selectImageFromGallery = await _picker.pickMultiImage();
    if(null!=_provider.selectImageFromGallery && _provider.selectImageFromGallery!.isNotEmpty){
      model.addMultipleImages(_provider.selectImageFromGallery!);
    }
    print('image path==${_provider.imageNames.length}');
    // model.imageNames.addAll(image!);
    // isCamera=true;
    //_imageFileList = image;
    // model.imageNames.add = image!;
    // model.imageNames.insert(0, _image!);
    // model.imageNamesCount!.insert(0,0);
    // model.imageNames = image!;

  }

  /*
  FileType _pickingType = FileType.image;
  ____selectImagesFromGallery() async {
    // setState(() => _loadingPath = true);
    try {
      _directoryPath = null;
      galleryImagesList = (await FilePicker.platform.pickFiles(
        type: _pickingType,
        // type: FileType.image,
        allowMultiple: true,
        onFileLoading: (FilePickerStatus status) => print(status),
        allowedExtensions: (_extension?.isNotEmpty ?? false)
            ? _extension?.replaceAll(' ', '').split(',')
            : null,
      ))?.files;
      if (galleryImagesList != null) {
        setState(() {
          isGallery=true;
          galleryImageFile = File(galleryImagesList.toString());
          print('path gallery image');
          print(Image.file(File(_pickingType.toString())));
        });
      }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    } catch (ex) {
      print(ex);
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
      _fileName =
      galleryImageFile != null ? galleryImagesList!.map((e) => e.name).toString() : '...';
      //print('gallery images paths' + '$galleryImageFile');
      galleryPath = galleryImagesList!.map((e) => e.path).toList().toString();
      print('galleryPath=========== $galleryPath');
    });
  }
   */
  pdfPicker(UploadNewDocumentProvider model) async {
    model.deleteAllImages();
    // setState(() => _loadingPath = true);
    try {
      _directoryPath = null;
      var selectedPdf = (await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowMultiple: false,
        onFileLoading: (FilePickerStatus status) => print(status),
        allowedExtensions: ['pdf'],
        // allowedExtensions: (_extension?.isNotEmpty ?? false)
        //     ? _extension?.replaceAll(' ', '').split(',')
        //     : null,
      )) ?.files;
      if(selectedPdf!.isNotEmpty)
        _provider.selectedPdfPath= selectedPdf[0];

    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    } catch (ex) {
      print(ex);
    }
    // if (!mounted) return;
    //   _loadingPath = false;
  }
  submitDocument() async{

    String? filePath;
    if(_provider.imageNames.isNotEmpty)
      filePath = await getUploadFilePath();
    else
      filePath = _provider.selectedPdfPath!.path;
    await insertDocumentsToDB(widget.env, descriptionController.text, filePath);
    GlobalProvider.of(context).uploadDataToServer();
    Navigator.pop(context, true);
    await FirebaseAnalytics.instance
        .logEvent(
      name: 'uploadDocuments',
      // parameters: {
      //   'id': 'nishigandha',
      // }
    );
    GlobalProvider.of(context).insertActivitesToDB(AppConstants.activityUploadDoc,AppConstants.activityUploadDoc,widget.env);

    //Navigator.of(context).pop();
  }
  Future insertDocumentsToDB(env, description, filePath) async {
    String? loginEmail = await SharedPreferenceUtils.getStringPreference(
        SharedPreferenceUtils.EMAIL_ID);
    final uploadNewDocumentDBModel= UploadNewDocumentDBModel(
      dId: 0,
      requestNo:env,
      apptId: 0,
      description: description ?? '',
      executorName: loginEmail.toString(),
      pdfUrl: '',
      localPdfUrl: filePath,
      uploadedDate: DateTime.now().toString(),
      syncStatus: 0,

    );

    await DBOperations().insertDocuments(uploadNewDocumentDBModel);
    print('database insert comments');
  }

  Future<String> getUploadFilePath() async{

    Directory appDocDir = await getApplicationDocumentsDirectory();
    final pdf = pw.Document();

    if(_provider.imageNames.isNotEmpty) {
      _provider.imageNames.forEach((element) {
        final image = pw.MemoryImage(
          File(element.path).readAsBytesSync(),
        );

        pdf.addPage(pw.Page(build: (pw.Context context) {
          return pw.Center(
            child: pw.Image(image),
          ); // Center
        }));
      });
    }

    final pdfPath= '${appDocDir.path}/${widget.env}${DateTime.now().millisecondsSinceEpoch}.pdf';
    print('pdfPath== $pdfPath');
    final file = File(pdfPath);
    await file.writeAsBytes(await pdf.save());
    return pdfPath;
  }
}

