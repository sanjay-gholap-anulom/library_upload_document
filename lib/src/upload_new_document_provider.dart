import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';

class UploadNewDocumentProvider extends ChangeNotifier{
  UploadNewDocumentProvider(){

  }
   List<XFile> _imageNames = <XFile>[];
   List<XFile> get imageNames => _imageNames;

   void addImage(XFile xfiles){
     _imageNames.add(xfiles);
     notifyListeners();
   }
   void addMultipleImages(List<XFile> xfiles){
     _imageNames.addAll(xfiles);
     notifyListeners();
   }

  List<int> _imageNamesCount = <int>[];
   List<int>? get imageNamesCount => _imageNamesCount;
   set imageNamesCount(List<int>? value) {
     _imageNamesCount = value!;
     notifyListeners();
   }

   String? _fileName;
   String? get fileName => _fileName;
  set fileName(String? value) {
    _fileName = value;
    notifyListeners();
  }
   PlatformFile? _selectedPdfPath;


   PlatformFile? get selectedPdfPath => _selectedPdfPath;

  set selectedPdfPath(PlatformFile? value) {
    _selectedPdfPath = value;
    notifyListeners();
  }

  List<XFile>? _selectImageFromGallery;

   List<XFile>? get selectImageFromGallery => _selectImageFromGallery;

  set selectImageFromGallery(List<XFile>? value) {
    _selectImageFromGallery = value;
    notifyListeners();
  }


  // void addPath(List<PlatformFile> platformFile){
  //     _selectedPdfPath!.addAll(platformFile);
  //     notifyListeners();
  //   }
   void deletePdfFile() {
     _selectedPdfPath=null;
     notifyListeners();
   }

   void deleteImages(int ref) {
     if(_imageNames.isNotEmpty && _imageNames.length > ref) {
       _imageNames.removeAt(ref).path;
       notifyListeners();
     }
   }
   void deleteAllImages() {
     if(_imageNames.isNotEmpty) {
       _imageNames.clear();
       notifyListeners();
     }
   }

}